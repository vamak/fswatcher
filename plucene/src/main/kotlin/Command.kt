
sealed class Command {
    object Error: Command()
    object PrintHelp: Command()
    object Quit: Command()
    object ListFolders: Command()
    object Status: Command()
    data class AddFolder(val path: String): Command()
    data class RemoveFolder(val path: String): Command()
    data class Peek(val token: String): Command()
    data class Watch(val token: String): Command()
    data class StopWatching(val token: String): Command()
    object Nop: Command()
}

fun parseInput(data: String): Command {
    val entries = data.split(' ').filter { it.isNotEmpty() }
    return if (entries.isEmpty()) {
        Command.Nop
    } else {
        val args = entries.drop(1)
        when (entries.first()) {
            "help" -> Command.PrintHelp
            "quit" -> Command.Quit
            "list" -> Command.ListFolders
            "status" -> Command.Status
            "add" -> tryParseAsAddFolder(args)
            "remove" -> tryParseAsRemoveFolder(args)
            "peek" -> tryParseAsPeek(args)
            "watch" -> tryParseAsWatch(args)
            "unwatch" -> tryParseAsStopWatching(args)
            else -> Command.Error
        }
    }
}

private fun tryParseAsAddFolder(args: List<String>): Command {
    return if (args.isNotEmpty()) {
        Command.AddFolder(args.first())
    } else {
        Command.Error
    }
}

private fun tryParseAsRemoveFolder(args: List<String>): Command {
    return if (args.isNotEmpty()) {
        Command.RemoveFolder(args.first())
    } else {
        Command.Error
    }
}

private fun tryParseAsPeek(args: List<String>): Command {
    return if (args.isNotEmpty()) {
        Command.Peek(args.first())
    } else {
        Command.Error
    }
}

private fun tryParseAsWatch(args: List<String>): Command {
    return if (args.isNotEmpty()) {
        Command.Watch(args.first())
    } else {
        Command.Error
    }
}

private fun tryParseAsStopWatching(args: List<String>): Command {
    return if (args.isNotEmpty()) {
        Command.StopWatching(args.first())
    } else {
        Command.Error
    }
}