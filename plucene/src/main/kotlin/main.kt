import com.vadmak.indexer.Indexer
import com.vadmak.indexer.IndexerImpl
import com.vadmak.indexer.Matcher
import io.reactivex.rxjava3.disposables.Disposable
import java.lang.Exception
import java.time.LocalTime
import java.time.temporal.ChronoUnit
import java.util.*

fun printHelp() {
    println("""
        Commands available here:
        
            help - Print this help
        
            quit - Exit this REPL
        
            list - list all the currently watched folders
            
            status - print index status: files which were not indexed + amount of files indexed !Caution! It might be very long!
        
            add FOLDER - recursively add the FOLDER into index. FOLDER must be an absolute path to existing folder.
            
            remove FOLDER - remove folder from index. FOLDER must be a folder existing in index.
        
            peek TOKEN - list files that contain TOKEN. Takes only one snapshot. 
        
            watch TOKEN - start watching registered folders and list files that contain TOKEN everytime there is a change. 
        
            unwatch TOKEN - stop watching the registered folders by specified TOKEN.
            
        In case command expects an argument, and more than one is provided, then only first argument is taken, the rest are ignored.
    """.trimIndent())
}

fun printError() {
    println("""
        Invalid command.
        help - will print help;
        quit - will quit this REPL
    """.trimIndent())
}

fun getUserHomeFolder() = System.getProperty("user.home")

class Context private constructor(
    private var keepGoing: Boolean,
    private val indexer: Indexer,
    private val watchSubscriptions: MutableMap<String, Disposable>
) {
    constructor(): this(true, IndexerImpl.construct(), mutableMapOf())

    val mayProceed: Boolean
        get() = keepGoing

    fun quit() {
        indexer.close()
        keepGoing = false
    }

    fun addFolder(command: Command.AddFolder) {
        val path = command.path.replace("~", getUserHomeFolder())
        println("-- Adding folder '$path'")
        val timeStarted = LocalTime.now()

        indexer.addFolder(path).blockingSubscribe(
            {
                val diff = timeStarted.until(LocalTime.now(), ChronoUnit.MILLIS).toFloat() / 1000
                println("-- Folder '$path' added, duration time $diff sec")
            },
            { e -> println("-- Could not add folder: $e") }
        )
    }

    fun removeFolder(command: Command.RemoveFolder) {
        val path = command.path.replace("~", getUserHomeFolder())
        println("--- Removing folder '$path'")
        val timeStarted = LocalTime.now()

        indexer.removeFolder(path).blockingSubscribe(
            {
                val diff = timeStarted.until(LocalTime.now(), ChronoUnit.MILLIS).toFloat() / 1000
                println("-- Folder '$path' removed, duration time $diff sec")
            },
            { e -> println("-- Could not remove folder: $e")}
        )
    }

    fun listFolders() {
        println("Added folders:")
        indexer.listAddedFolders()
            .blockingSubscribe(
                { folder -> println(folder) },
                { e -> println("Error: $e") },
                { println("---------------") }
            )
    }

    fun peekFolder(command: Command.Peek) {
        indexer.search(Matcher.PlainWord(command.token))
            .firstOrError()
            .blockingSubscribe(
                { item -> println("Peek '${command.token}' = $item")},
                { error -> println("Error $error") }
            )
    }

    fun printStatus() {
        println("Status:")

        val statusPrinter = { status :Indexer.IndexStatus ->
            println("Amount of indexed files = ${status.amountOfIndexed}")
            println("Rejected files: ${status.rejected.size}. Put a breakpoint in main, if you need to examine")
            println("Unreachable files: ${status.unreachable.size}. Put a breakpoint in main, if you need to examine")
        }

        indexer.status()
            .blockingSubscribe(
                statusPrinter,
                { e -> println("Error: $e") },
            )
    }

    fun watchFolder(command: Command.Watch) {
        if (!watchSubscriptions.containsKey(command.token)) {
            val disposable = indexer.search(Matcher.PlainWord(command.token))
                .doOnDispose {
                    println("No longer watching '${command.token}'")
                }
                .subscribe(
                    {item ->
                        println("Watch '${command.token}': $item")
                    },
                    {error ->
                        println("Watch '${command.token}': Error = $error")
                        watchSubscriptions.remove(command.token)
                    },
                    {
                        println("No longer watching '${command.token}'")
                        watchSubscriptions.remove(command.token)
                    }
                )

            watchSubscriptions[command.token] = disposable
        }
    }

    fun stopWatching(command: Command.StopWatching) {
        println("Asked to stop watching ${command.token}")
        watchSubscriptions.remove(command.token)?.dispose()
    }
}

fun main() {
    println("Welcome to plucene REPL.")
    println("'help' will print help, 'quit' will exit this REPL")

    val context = Context()

    Runtime.getRuntime().addShutdownHook(Thread {
        println("Terminating")
        context.quit()
    })

    while (context.mayProceed) {
        print(">")
        val enteredCommand = readLine()
        val command = if (enteredCommand != null) {
            parseInput(enteredCommand)
        } else {
            Command.Nop
        }

        when (command) {
            Command.Error -> printError()
            Command.PrintHelp -> printHelp()
            Command.Quit -> context.quit()
            Command.ListFolders -> context.listFolders()
            Command.Status -> context.printStatus()
            is Command.AddFolder -> context.addFolder(command)
            is Command.RemoveFolder -> context.removeFolder(command)
            is Command.Peek -> context.peekFolder(command)
            is Command.Watch -> context.watchFolder(command)
            is Command.StopWatching -> context.stopWatching(command)
            Command.Nop -> Unit
        }
    }
}