
dependencies {
    api("io.reactivex.rxjava3:rxjava:3.0.6")
    implementation("org.apache.tika:tika-parsers:1.24")
    implementation("org.slf4j:slf4j-api:1.7.5")
    implementation("org.slf4j:slf4j-log4j12:1.7.5")

    // https://www.javaxt.com/javaxt-core/
    implementation(files("lib/javaxt-core_v1.10.3/bin/javaxt-core.jar"))


    testImplementation("org.junit.jupiter:junit-jupiter-api:5.3.1")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.3.1")
    testImplementation("org.mockito:mockito-core:2.1.0")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.1.0")
}
