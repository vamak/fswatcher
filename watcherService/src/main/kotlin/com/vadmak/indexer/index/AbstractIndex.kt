package com.vadmak.indexer.index

import com.vadmak.indexer.tokens.Predicate
import com.vadmak.indexer.tokens.Tokenizer
import com.vadmak.indexer.fs.FileSystem
import java.nio.file.Path

internal abstract class AbstractIndex(
    protected val fileSystem: FileSystem,
    protected val directories: HashMap<Path, DirectoryIndex>,
    protected val tokenizer: Tokenizer,
    protected val addedRootFolders: MutableSet<Path>
) {
    internal enum class FileState {
        Visited,
        ToBeVisited,
        Unreachable,
        Rejected,
    }
    internal data class DirectoryIndex(
        val subfolders: Set<Path> = emptySet(),
        val fileStates: Map<Path, FileState> = HashMap(),
        val invertedTokenIndex: Map<String, Set<Path>> = HashMap(),
    ) {
        fun filesForPredicate(predicate: Predicate, directoryPath: Path): Set<Path> {
            val resultingSet = HashSet<Path>()

            invertedTokenIndex.filterKeys { predicate.hasMatch(it) }
                .flatMapTo(resultingSet) { (_, paths) -> paths }

            return resultingSet
                .map { directoryPath.resolve(it) }
                .toHashSet()
        }
    }

    companion object {
        // Not sure how good this idea is. Needed for testing
        internal fun accessIndex(instance: AbstractIndex): Map<Path, DirectoryIndex> {
            return instance.directories
        }
    }
}