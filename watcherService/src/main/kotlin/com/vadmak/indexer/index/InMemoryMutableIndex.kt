package com.vadmak.indexer.index

import com.vadmak.indexer.tokens.TokenizationResult
import com.vadmak.indexer.tokens.Tokenizer
import com.vadmak.indexer.fs.FileSystem
import com.vadmak.indexer.modifications.FsEvent
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.nio.file.Path
import java.time.LocalDateTime

internal class InMemoryMutableIndex(
    fileSystem: FileSystem,
    directories: HashMap<Path, DirectoryIndex>,
    tokenizer: Tokenizer,
    addedRootFolders: MutableSet<Path>
): AbstractIndex(fileSystem, directories, tokenizer, addedRootFolders), MutableIndex {
    override fun commit(): Index {
        return InMemoryIndex(fileSystem, directories, tokenizer, addedRootFolders)
    }

    override fun applyEvent(event: IndexMutationEvent) {
        when (event) {
            is IndexMutationEvent.ArtificialFolderAdd -> folderAddedArtificially(event.path)
            is IndexMutationEvent.ArtificialFolderRemove -> folderRemovedArtificially(event.path)
            is IndexMutationEvent.FromFileSystem -> event.events.forEach(::applyFsEvent)
        }
    }

    override fun visitUnvisited(scheduler: Scheduler) {
        Observable.fromIterable(directories.entries)
            .flatMapSingle { dirEntry ->
                // by doing this trick we run lots of tasks in parallel
                visitDirectory(dirEntry.toPair()).subscribeOn(scheduler)
            }
            .scan(directories) { currentMasterIndex, (path, index) ->
                currentMasterIndex.also {
                    it[path] = index
                }
            }
            .ignoreElements()
            .blockingAwait()
    }

    private fun mergeTokens(tokenIndex: MutableMap<String, Set<Path>>, localFilePath: Path, newTokens: Set<String>) {
        newTokens.forEach { token ->
            tokenIndex.compute(token) { _, paths ->
                if (paths != null) {
                    paths.toMutableSet().also { it.add(localFilePath) }
                } else {
                    setOf(localFilePath)
                }
            }
        }
    }

    private fun visitDirectory(mapEntry: Pair<Path, DirectoryIndex>): Single<Pair<Path, DirectoryIndex>> =
        Single.create { emitter ->
            val (dirPath, dirIndex) = mapEntry

            if (dirIndex.fileStates.any { (_, state) -> state == FileState.ToBeVisited }) {
                val newFileStates = dirIndex.fileStates.toMutableMap()
                val newTokens = dirIndex.invertedTokenIndex.toMutableMap()

                dirIndex.fileStates.filterValues { it == FileState.ToBeVisited }
                    .forEach { (fileLocalPath, _) ->
                        val fileAbsolutePath = dirPath.resolve(fileLocalPath)
                        when (val visitResult = tokenizer.tokenize(fileAbsolutePath)) {
                            TokenizationResult.Unreachable -> newFileStates[fileLocalPath] = FileState.Unreachable
                            TokenizationResult.Rejected -> newFileStates[fileLocalPath] = FileState.Rejected
                            is TokenizationResult.Success -> {
                                mergeTokens(newTokens, fileLocalPath, visitResult.tokens)
                                newFileStates[fileLocalPath] = FileState.Visited
                            }
                        }
                    }

                val newDirIndex = dirIndex.copy(
                    fileStates = newFileStates,
                    invertedTokenIndex = newTokens
                )
                emitter.onSuccess(dirPath to newDirIndex)
            } else {
                emitter.onSuccess(mapEntry)
            }
        }

    private fun applyFsEvent(fsEvent: FsEvent) {
        when (fsEvent.type) {
            FsEvent.Type.Created -> entryCreated(fsEvent.absolutePath)
            FsEvent.Type.Deleted -> entryDeleted(fsEvent.absolutePath)
            FsEvent.Type.Modified -> entryModified(fsEvent.absolutePath)
        }
    }

    private fun entryDeleted(absolutePath: Path) {
        if (directories.containsKey(absolutePath)) {

            // looks like it was a directory
            val parentPath = absolutePath.parent
            val localPath = absolutePath.last()
            directories.computeIfPresent(parentPath) { _, parentIndex ->
                val newSubfolders = parentIndex.subfolders.toMutableSet()
                newSubfolders.remove(localPath)

                parentIndex.copy(
                    subfolders = newSubfolders
                )
            }
            folderDeleted(absolutePath)
        } else {
            // lookup at its folder
            val parentPath = absolutePath.parent
            val localPath = absolutePath.last()
            directories.computeIfPresent(parentPath) { _, directoryIndex ->
                val newFiles = directoryIndex.fileStates.filterKeys { it != localPath  }
                val newTokens = directoryIndex.invertedTokenIndex.mapValues { (_, paths) ->
                    paths.filter { it != localPath }
                        .toSet()
                }.filterValues { it.isNotEmpty() }

                directoryIndex.copy(fileStates = newFiles, invertedTokenIndex = newTokens)
            }
        }
    }

    private fun folderRemovedArtificially(absolutePath: Path) {
        addedRootFolders.remove(absolutePath)

        fun removeRecursively(pathToBeRemoved: Path) {
            // if there is no other added folder to which current path is a subfolder we proceed with deletion
            if (addedRootFolders.none { pathToBeRemoved.startsWith(it) }) {

                directories.computeIfPresent(pathToBeRemoved.parent) { _, dirIndex ->
                    val newSubfolders = dirIndex.subfolders.toMutableSet()
                    newSubfolders.remove(pathToBeRemoved.last())

                    dirIndex.copy(subfolders = newSubfolders)
                }

                directories.remove(pathToBeRemoved)?.subfolders?.forEach {
                    removeRecursively(pathToBeRemoved.resolve(it))
                }
            }
        }

        removeRecursively(absolutePath)
    }

    private fun folderDeleted(absolutePath: Path) {
        directories.remove(absolutePath)?.subfolders?.forEach {
            folderDeleted(absolutePath.resolve(it))
        }
    }

    private fun entryModified(absolutePath: Path) {
        if (!fileSystem.isSymlink(absolutePath)) {
            if (fileSystem.isDirectory(absolutePath)) {

                // Normally this should be ignored, except for the cases when folder got permissions/owner changed
                if (!directories.containsKey(absolutePath)) {
                    folderCreated(absolutePath)
                }
            } else {
                val parent = absolutePath.parent
                val localPath = absolutePath.last()
                directories.computeIfPresent(parent) { _, directoryIndex ->
                    val newFiles = directoryIndex.fileStates.toMutableMap().also {
                        it[localPath] = FileState.ToBeVisited
                    }
                    val newTokens = directoryIndex.invertedTokenIndex.mapValues { (_, paths) ->
                        paths.filter { it != localPath }
                            .toSet()
                    }.filterValues { it.isNotEmpty() }

                    directoryIndex.copy(fileStates = newFiles, invertedTokenIndex = newTokens)
                }
            }
        }
    }

    private fun entryCreated(absolutePath: Path) {
        if (!fileSystem.isSymlink(absolutePath)) {
            if (fileSystem.isDirectory(absolutePath)) {
                folderCreated(absolutePath)
            } else {

                val parent = absolutePath.parent
                val localPath = absolutePath.last()
                directories.computeIfPresent(parent) { _, directory ->
                    val newFiles = directory.fileStates.toMutableMap().also {
                        it[localPath] = FileState.ToBeVisited
                    }

                    directory.copy(fileStates = newFiles)
                }
            }
        }
    }

    private fun folderAddedArtificially(absolutePath: Path) {
        // if no previously added folder is parent of this one
        if (addedRootFolders.none { absolutePath.startsWith(it) }) {
            folderCreated(absolutePath)
        }
        addedRootFolders.add(absolutePath)
    }

    private fun folderCreated(absolutePath: Path) {
        val localPath = absolutePath.last()
        directories.computeIfPresent(absolutePath.parent) { _, index ->
            val newSubfolders = index.subfolders.toMutableSet()
            newSubfolders.add(localPath)

            index.copy(subfolders = newSubfolders)
        }

        folderCreatedRecursive(absolutePath)
    }

    private fun folderCreatedRecursive(absolutePath: Path) {
        val (files, folders) = fileSystem.exploreFolder(absolutePath)
        val newFileStates = files
            .map { it.last() }
            .associateWith { FileState.ToBeVisited }

        directories[absolutePath] = DirectoryIndex(
            subfolders = folders.map { it.last() }.toHashSet(),
            fileStates = newFileStates
        )

        folders.forEach(::folderCreated)
    }
}