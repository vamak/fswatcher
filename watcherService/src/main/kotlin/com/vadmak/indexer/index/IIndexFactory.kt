package com.vadmak.indexer.index

internal interface IndexFactory {
    fun construct(): Index
}