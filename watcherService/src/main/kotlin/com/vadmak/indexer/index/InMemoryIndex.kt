package com.vadmak.indexer.index

import com.vadmak.indexer.Indexer
import com.vadmak.indexer.tokens.Predicate
import com.vadmak.indexer.tokens.Tokenizer
import com.vadmak.indexer.tokens.TokenizerFactory
import com.vadmak.indexer.fs.FileSystem
import java.nio.file.Path

internal class InMemoryIndexFactory(
    private val fileSystem: FileSystem,
    private val tokenizerFactory: TokenizerFactory,
): IndexFactory {
    override fun construct(): Index {
        return InMemoryIndex.createEmpty(fileSystem, tokenizerFactory.construct())
    }
}

internal class InMemoryIndex(
    fileSystem: FileSystem,
    directories: HashMap<Path, DirectoryIndex>,
    tokenizer: Tokenizer,
    addedRootFolders: MutableSet<Path>
): AbstractIndex(fileSystem, directories, tokenizer, addedRootFolders), Index {

    companion object {
        fun createEmpty(fileSystem: FileSystem, tokenizer: Tokenizer): Index {
            return InMemoryIndex(fileSystem, HashMap(), tokenizer, HashSet())
        }
    }

    override fun toSnapshot(predicate: Predicate): Indexer.Snapshot {
        val files = directories.asSequence()
            .flatMap { (directoryPath, directoryIndex) ->
                directoryIndex.filesForPredicate(predicate, directoryPath)
            }.map {
                it.toString()
            }

        return Indexer.Snapshot(files.toList())
    }

    override fun toIndexStatus(): Indexer.IndexStatus {
        val unreachable = mutableSetOf<String>()
        val rejected = mutableSetOf<String>()

        directories.entries.forEach { (dirPath, dirIndex) ->
            dirIndex.fileStates.entries.forEach { (fileRelativePath, fileStatus) ->
                if (fileStatus == FileState.Unreachable) {
                    unreachable.add(dirPath.resolve(fileRelativePath).toString())
                } else if (fileStatus == FileState.Rejected) {
                    rejected.add(dirPath.resolve(fileRelativePath).toString())
                }
            }
        }
        val indexedFiles = directories.entries.map {(_, dirIndex) ->
            dirIndex.fileStates.filterValues { it == FileState.Visited }.size
        }.sum()

        return Indexer.IndexStatus(unreachable, rejected, indexedFiles)
    }

    override fun startMutation(): MutableIndex {
        val copiedIndexShallow = HashMap(directories)
        return InMemoryMutableIndex(fileSystem, copiedIndexShallow, tokenizer, addedRootFolders.toMutableSet())
    }
}

