package com.vadmak.indexer.index

import com.vadmak.indexer.Indexer
import com.vadmak.indexer.tokens.Predicate
import com.vadmak.indexer.modifications.FsEvent
import io.reactivex.rxjava3.core.Scheduler
import java.nio.file.Path

internal sealed class IndexMutationEvent {
    data class ArtificialFolderAdd(val path: Path, val onFolderAdded: () -> Unit): IndexMutationEvent()
    data class ArtificialFolderRemove(val path: Path, val onFolderRemoved: () -> Unit): IndexMutationEvent()
    data class FromFileSystem(val events: List<FsEvent>): IndexMutationEvent()
}

internal interface Index {
    fun toSnapshot(predicate: Predicate): Indexer.Snapshot

    fun startMutation(): MutableIndex

    fun toIndexStatus(): Indexer.IndexStatus
}

internal interface MutableIndex {
    fun commit(): Index

    fun applyEvent(event: IndexMutationEvent)
    fun visitUnvisited(scheduler: Scheduler)
}

