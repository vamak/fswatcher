package com.vadmak.indexer

import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import java.io.Closeable

sealed class Matcher {
    /**
     * Token matches the criteria if it equals to specified case-sensitive word.
     */
    data class PlainWord(val word: String): Matcher()
}

interface Indexer: Closeable {
    // contains absolute paths to files
    data class Snapshot(val files: List<String>)

    // contains absolute paths to files which are not tokenized
    data class IndexStatus(val unreachable: Set<String>, val rejected: Set<String>, val amountOfIndexed: Int)

    /**
     *  Recursively adds folder to index.
     *
     *  @param folder
     *         absolute path to watched folder.
     *
     *  @return a completable object which completes when the specified folder is added and fully indexed
     *      may throw {code InvalidPathException} in case provided folder is invalid or is not a directory
     */
    fun addFolder(folder: String): Completable

    /**
     * Asks for list of added folders.
     *
     * @return an observable with folder paths. No specific emitting order.
     */
    fun listAddedFolders(): Observable<String>

    /**
     * Removes provided folder from index.
     *
     * @return a completable object which completes when specified folder is removed, and index is adjusted
     *         may throw {code NoSuchElementException} in case provided folder is not found.
     */
    fun removeFolder(folder: String): Completable

    /**
     *  @return Observable which delivers snapshots with files that satisfy the matcher
     *          Will complete after the service is closed.
     *          By default notifications happen on Schedulers.computation(), but this can be changed with `observeOn`
     */
    fun search(matcher: Matcher): Observable<Snapshot>

    /**
     * @return State of index in regards to unindexed files.
     */
    fun status(): Single<IndexStatus>
}