package com.vadmak.indexer.fs

import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.WatchService

internal interface FileSystem {
    fun newWatchService(directory: Path): WatchService

    fun createDirectory(path: String): javaxt.io.Directory

    fun isDirectory(path: Path): Boolean

    fun isSymlink(path: Path): Boolean

    fun getPath(path: String): Path

    fun walkFileTree(rootDirectory: Path, fileVisitor: SimpleFileVisitor<Path>)

    data class FolderExploreResult(val files: Sequence<Path>, val folders: Sequence<Path>)
    fun exploreFolder(rootDirectory: Path): FolderExploreResult
}