package com.vadmak.indexer.fs

import javaxt.io.Directory
import java.io.IOException
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes

internal class FileSystemImpl: FileSystem {
    override fun newWatchService(directory: Path): WatchService =
        directory.fileSystem.newWatchService()

    override fun createDirectory(path: String): Directory = Directory(path)

    override fun isDirectory(path: Path): Boolean = Files.isDirectory(path)

    override fun isSymlink(path: Path): Boolean = Files.isSymbolicLink(path)

    override fun getPath(path: String): Path = Paths.get(path)

    override fun walkFileTree(rootDirectory: Path, fileVisitor: SimpleFileVisitor<Path>) {
        Files.walkFileTree(rootDirectory, fileVisitor)
    }

    override fun exploreFolder(rootDirectory: Path): FileSystem.FolderExploreResult {
        val listOfFiles = HashSet<Path>()
        val listOfFolders = HashSet<Path>()

        Files.walkFileTree(rootDirectory, mutableSetOf(), 1, object: SimpleFileVisitor<Path>() {
            override fun visitFile(file: Path, attrs: BasicFileAttributes?): FileVisitResult {
                if (!isSymlink(file)) {
                    if (isDirectory(file)) {
                        listOfFolders.add(file)
                    } else {
                        listOfFiles.add(file)
                    }
                }
                return FileVisitResult.CONTINUE
            }

            override fun visitFileFailed(file: Path?, exc: IOException?): FileVisitResult {
                return FileVisitResult.CONTINUE
            }
        })

        return FileSystem.FolderExploreResult(
            files = listOfFiles.asSequence(),
            folders = listOfFolders.asSequence()
        )
    }
}