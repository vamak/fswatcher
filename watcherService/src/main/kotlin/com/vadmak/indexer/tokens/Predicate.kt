package com.vadmak.indexer.tokens

import com.vadmak.indexer.Matcher

internal interface Predicate {
    fun hasMatch(token: String): Boolean
}

internal interface PredicateFactory {
    fun construct(matcher: Matcher): Predicate
}

internal class ByWordPredicate(val matcher: Matcher.PlainWord): Predicate {
    override fun hasMatch(token: String): Boolean {
        return token == matcher.word
    }
}

internal class PredicateFactoryImpl: PredicateFactory {
    override fun construct(matcher: Matcher): Predicate {
        return when (matcher) {
            is Matcher.PlainWord -> ByWordPredicate(matcher)
        }
    }
}