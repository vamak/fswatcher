package com.vadmak.indexer.tokens

import java.io.BufferedReader
import java.io.FileReader
import java.lang.StringBuilder
import java.nio.file.Path
import java.util.*
import java.util.regex.Pattern

internal class TokenizerByWordFactory: TokenizerFactory {
    override fun construct(): Tokenizer = BufferedTokenizer(true)
}

internal class BufferedTokenizer(private val probingEnabled: Boolean): Tokenizer {
    override fun tokenize(file: Path): TokenizationResult {
        return try {
            unsafeTokenize(file)
        } catch (e: Exception) {
            TokenizationResult.Unreachable
        }
    }

    companion object {
        private const val READ_BUFFER_SIZE = 10240
        private const val TOO_LONG_TO_BE_WORD = 5 * 1024
        private val DELIMITERS = " ,./?\\\'\"|:;[]{}!()<>=+\r\n\t*&^".toSortedSet().toCharArray()
        private val delimiterPattern = Pattern.compile("[\\s,.!:;?\'\"\\[\\](){}<>=+]")
        private const val LEAST_AMOUNT_TOKENS = 2
    }

    // hence it's a short CharArray, this thing works pretty fast
    private fun Char.isDelimiter() = DELIMITERS.contains(this)

    private fun extractWords(from: CharArray, len: Int, carriedFromLast: String, to: MutableSet<String>): String {
        val currentWord = StringBuilder()
        var positionCarried = 0
        var positionFrom = 0

        while (positionFrom < len) {
            val element = if (positionCarried < carriedFromLast.length) {
                carriedFromLast[positionCarried++]
            } else {
                from[positionFrom++]
            }

            if (element.isDelimiter()) {
                if (currentWord.isNotEmpty()) {
                    if (currentWord.length < TOO_LONG_TO_BE_WORD) {
                        to.add(currentWord.toString())
                    }
                    currentWord.clear()
                }
            } else {
                currentWord.append(element)
            }
        }

        if (currentWord.length < TOO_LONG_TO_BE_WORD) {
            return currentWord.toString()
        } else {
            return ""
        }
    }

    fun unsafeTokenize(file: Path): TokenizationResult {
        if (!unsafeIsTextFile(file)) {
            return TokenizationResult.Rejected
        }

        val result = mutableSetOf<String>()

        val buffer = CharArray(READ_BUFFER_SIZE)
        var carryOver: String = ""

        BufferedReader(FileReader(file.toFile())).use { reader ->
            do {
                val charsRead = reader.read(buffer, 0, READ_BUFFER_SIZE)
                carryOver = extractWords(buffer, charsRead, carryOver, result)
            } while (charsRead > 0)
        }

        if (carryOver.isNotEmpty()) {
            result.add(carryOver)
        }

        return TokenizationResult.Success(result)
    }

    // may throw
    private fun unsafeIsTextFile(file: Path): Boolean {
        Scanner(file.toFile()).useDelimiter(delimiterPattern).use { scanner ->
            var tokensRead = 0
            while (scanner.hasNext() && (tokensRead < LEAST_AMOUNT_TOKENS)) {
                scanner.next()
                tokensRead += 1
            }

            return (tokensRead == LEAST_AMOUNT_TOKENS)
        }
    }
}