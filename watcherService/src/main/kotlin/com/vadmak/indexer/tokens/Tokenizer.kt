package com.vadmak.indexer.tokens

import java.nio.file.Path

sealed class TokenizationResult {
    data class Success(val tokens: Set<String>): TokenizationResult()
    object Unreachable: TokenizationResult()
    object Rejected: TokenizationResult()
}

internal interface Tokenizer {
    /**
     * Extract tokens from the given file
     *
     * @param file - absolute path to the file
     *
     * @return  TokenizationResult.Success - in case file was successfully tokenized
     *          TokenizationResult.Unreachable - in case file can not be read (no rights, or it does not exist)
     *          TokenizationResult.Rejected - in case file is considered a binary one.
     */
    fun tokenize(file: Path): TokenizationResult
}

internal interface TokenizerFactory {
    fun construct(): Tokenizer
}