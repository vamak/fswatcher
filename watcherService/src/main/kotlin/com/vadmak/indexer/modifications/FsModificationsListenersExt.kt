package com.vadmak.indexer.modifications

import com.vadmak.indexer.fs.FileSystem
import com.vadmak.indexer.fs.FileSystemImpl
import io.reactivex.rxjava3.core.Emitter
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.functions.BiConsumer
import io.reactivex.rxjava3.schedulers.Schedulers
import javaxt.io.Directory
import java.nio.file.Path

internal class FsModificationsListenersExt constructor(
    private val filesystem: FileSystem,
): FsModificationsListener {

    private fun Directory.Event.toFsEvent(filesystem: FileSystem): FsEvent? {
        val eventType = when(this.eventID) {
            Directory.Event.DELETE -> FsEvent.Type.Deleted
            Directory.Event.CREATE -> FsEvent.Type.Created
            Directory.Event.MODIFY -> FsEvent.Type.Modified
            else -> null
        }

        return eventType?.let {
            FsEvent(it, filesystem.getPath(this.file))
        }
    }

    override fun recursiveWatch(directory: String): Observable<List<FsEvent>> {
        return Observable.generate(
            { filesystem.createDirectory(directory) },
            stateMutator,
            { dir ->
                dir.stop()
            }
        ).subscribeOn(Schedulers.io())
    }

    companion object {
        fun constructService(): FsModificationsListener = FsModificationsListenersExt(FileSystemImpl())
    }

    private val stateMutator = BiConsumer { state: Directory, emitter: Emitter<List<FsEvent>> ->
        val unsafeEventCollection = state.events
        val receivedEvents = mutableListOf<Directory.Event?>()

        synchronized(unsafeEventCollection) {
            if(unsafeEventCollection.isEmpty()) {
                try {
                    (unsafeEventCollection as Object).wait(200)
                } catch (e: InterruptedException) {
                    // do nothing
                }
            }

            if (unsafeEventCollection.isNotEmpty()) {
                (0 until unsafeEventCollection.size).mapTo(receivedEvents) { unsafeEventCollection[it] as Directory.Event }
                unsafeEventCollection.clear()
            }
        }

        val convertedEvents = receivedEvents
            .asSequence()
            .filterNotNull()
            .mapNotNull {
                it.toFsEvent(filesystem)
            }
            .toList()

        if (convertedEvents.isNotEmpty()) {
            emitter.onNext(convertedEvents)
        }
    }
}