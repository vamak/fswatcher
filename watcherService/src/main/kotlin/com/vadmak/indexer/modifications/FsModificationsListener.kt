package com.vadmak.indexer.modifications

import io.reactivex.rxjava3.core.Observable
import java.nio.file.Path

data class FsEvent(val type: Type, val absolutePath: Path) {
    enum class Type {
        Created,
        Deleted,
        Modified
    }
}

internal interface FsModificationsListener {
    /**
     * After subscribing the observable may throw:
     * * IOException, if @param directory points to nonexisting folder, or
     *                          user has no rights to access it.
     */
    fun recursiveWatch(directory: String): Observable<List<FsEvent>>
}