package com.vadmak.indexer

import com.vadmak.indexer.index.InMemoryIndexFactory
import com.vadmak.indexer.index.Index
import com.vadmak.indexer.index.IndexFactory
import com.vadmak.indexer.index.IndexMutationEvent
import com.vadmak.indexer.tokens.PredicateFactory
import com.vadmak.indexer.tokens.PredicateFactoryImpl
import com.vadmak.indexer.tokens.TokenizerByWordFactory
import com.vadmak.indexer.fs.FileSystem
import com.vadmak.indexer.fs.FileSystemImpl
import com.vadmak.indexer.modifications.FsEvent
import com.vadmak.indexer.modifications.FsModificationsListener
import com.vadmak.indexer.modifications.FsModificationsListenersExt
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import io.reactivex.rxjava3.subjects.PublishSubject
import java.nio.file.InvalidPathException
import java.nio.file.Path
import java.util.concurrent.Executors
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import kotlin.NoSuchElementException

class IndexerImpl internal constructor(
    private val fsModificationsListener: FsModificationsListener,
    private val fileSystem: FileSystem,
    private val indexFactory: IndexFactory,
    private val predicateFactory: PredicateFactory,
) : Indexer {
    private val indexMutationEventsHub = PublishSubject.create<IndexMutationEvent>()
    private val closeStream = PublishSubject.create<Unit>()
    private val indexStream = BehaviorSubject.create<WrappedIndex>()
    private val addedFolders = mutableMapOf<Path, Disposable>()

    private val threadPool = Executors.newWorkStealingPool() // with default level of parallelism
    private val schedulerForVisits = Schedulers.from(threadPool)

    init {
        indexMutationEventsHub
            .serialize()    // it's important, so that events come sequentially (same thread is not guaranteed though)
            .buffer(1, TimeUnit.SECONDS)    // to process more events per one index mutation
            .filter { it.isNotEmpty() }
            .observeOn(Schedulers.computation())
            .takeUntil(closeStream)
            .scan(
                initialIndex(),
                indexMutator(),
            )
            .subscribe(indexStream)

        indexStream
            .map { it.latestEvents }
            .subscribe(
                // here we notify all pending Add/Remove folder operations.
                // and guarantee that index is reevaluated by the time of notification
                { it.notifyAddRemove() },
                { _ -> Unit},
                // When stream completes, we need to shutdown everything
                { threadPool.shutdown() }
            )
    }

    override fun addFolder(folder: String): Completable {
        return Completable.create { emitter ->
            val path = verifyPath(folder)
            if (addedFolders.containsKey(path)) {
                // no need to do it again
                emitter.onComplete()
            } else {

                val folderSubscription = fsModificationsListener.recursiveWatch(folder)
                    .map { IndexMutationEvent.FromFileSystem(it) }
                    .onErrorResumeWith(Observable.never())  // just in case, not to screw the index
                    .takeUntil(closeStream)
                    .subscribe(
                        { it -> indexMutationEventsHub.onNext(it) },
                        { _ -> /* we ignore errors */ },
                        { indexMutationEventsHub.onComplete() }
                    )

                addedFolders[path] = folderSubscription

                indexMutationEventsHub.onNext(
                    IndexMutationEvent.ArtificialFolderAdd(path) {
                        emitter.onComplete()
                    }
                )
            }
        }.subscribeOn(Schedulers.single())  // it's important, cause `subscribe` and other internals are not thread-safe
    }

    override fun removeFolder(folder: String): Completable {
        return Completable.create { emitter ->
            val key = addedFolders.keys.find { it.toString() == folder }

            if (key != null) {
                addedFolders.remove(key)!!.dispose()

                indexMutationEventsHub.onNext(
                    IndexMutationEvent.ArtificialFolderRemove(key) {
                        emitter.onComplete()
                    }
                )

            } else {
                emitter.onError(NoSuchElementException())
            }
        }.subscribeOn(Schedulers.single())
    }

    override fun listAddedFolders(): Observable<String> {
        return Observable.create<String> { emitter ->
            addedFolders.keys.forEach {
                emitter.onNext(it.toString())
            }

            emitter.onComplete()
        }.subscribeOn(Schedulers.single())
    }

    override fun close() = closeStream.onNext(Unit)

    override fun search(matcher: Matcher): Observable<Indexer.Snapshot> {
        val predicate = predicateFactory.construct(matcher)

        return indexStream
            .map { it.index }
            .observeOn(Schedulers.computation())
            .map { it.toSnapshot(predicate) }
            .distinctUntilChanged()
    }

    override fun status(): Single<Indexer.IndexStatus> {
        return indexStream
            .map { it.index }
            .observeOn(Schedulers.computation())
            .map { it.toIndexStatus() }
            .firstOrError()
    }

    companion object {
        fun construct(): IndexerImpl {
            val fileSystem = FileSystemImpl()
            val tokenizerFactory = TokenizerByWordFactory()

            return IndexerImpl(
                FsModificationsListenersExt.constructService(),
                fileSystem,
                InMemoryIndexFactory(fileSystem, tokenizerFactory),
                PredicateFactoryImpl(),
            )
        }
    }

    private data class WrappedIndex(val index: Index, val latestEvents: List<IndexMutationEvent>)

    private fun verifyPath(folder: String): Path {
        val folderPath = fileSystem.getPath(folder)
        if (fileSystem.isDirectory(folderPath)) {
            return folderPath
        } else {
            throw InvalidPathException(folder, "is not an accessible folder")
        }
    }

    private fun initialIndex(): WrappedIndex {
        return WrappedIndex(indexFactory.construct(), emptyList())
    }

    private fun indexMutator() = BiFunction<WrappedIndex, List<IndexMutationEvent>, WrappedIndex> { wrappedIndex, events ->
        if (events.isNotEmpty()) {
            // 1. get Mutated Index
            val mutableIndex = wrappedIndex.index.startMutation()

            // 2. apply mutation events
            events.forEach { mutableIndex.applyEvent(it) }

            // 3. visit the unvisited
            mutableIndex.visitUnvisited(schedulerForVisits)

            // 4. return new index
            WrappedIndex(mutableIndex.commit(), events)
        } else {
            wrappedIndex
        }
    }

    private fun List<IndexMutationEvent>.notifyAddRemove() {
        this.filterIsInstance<IndexMutationEvent.ArtificialFolderAdd>()
            .forEach { it.onFolderAdded() }

        this.filterIsInstance<IndexMutationEvent.ArtificialFolderRemove>()
            .forEach { it.onFolderRemoved() }
    }
}