package com.vadmak.indexer.tokens

import com.vadmak.indexer.Matcher
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class PredicateFactoryImplTest {
    private val sut = PredicateFactoryImpl()

    @Test
    fun `PlainWord matcher results in ByWordPredicate`() {
        val result = sut.construct(Matcher.PlainWord(""))

        Assertions.assertTrue(result is ByWordPredicate)
    }
}