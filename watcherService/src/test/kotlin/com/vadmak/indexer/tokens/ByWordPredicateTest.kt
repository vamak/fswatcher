package com.vadmak.indexer.tokens

import com.vadmak.indexer.Matcher
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ByWordPredicateTest {
    private val sut = ByWordPredicate(Matcher.PlainWord("trololo"))

    @Test
    fun `empty string, gives no match`() {
        Assertions.assertFalse(sut.hasMatch(""))
    }

    @Test
    fun `same string different case, gives no match`() {
        Assertions.assertFalse(sut.hasMatch("TrOloLo"))
    }

    @Test
    fun `exactly same string, gives match`() {
        Assertions.assertTrue(sut.hasMatch("trololo"))
    }
}