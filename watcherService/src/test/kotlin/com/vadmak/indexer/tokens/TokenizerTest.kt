package com.vadmak.indexer.tokens

import org.junit.jupiter.api.*
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.nio.file.Files
import java.nio.file.Paths

class TokenizerByWordTest {
    val longTextpath = Paths.get("src/test/resources/book1").toAbsolutePath()
    val binaryFile = Paths.get("src/test/resources/libce-jni.so").toAbsolutePath()
    val codeFile = Paths.get("src/test/resources/TokenizerByWord.code").toAbsolutePath()

    private val sut = BufferedTokenizer(true)
    private lateinit var tempFile: File


    @BeforeEach
    fun setUp() {
        tempFile = File.createTempFile("trololo", null)
    }

    @AfterEach
    fun tearDown() {
        tempFile.delete()
    }

    fun writeLine(line: String) {
        BufferedWriter(FileWriter(tempFile)).use {
            it.write(line)
        }
    }

    @Test
    fun `empty file, returns Rejected`() {
        val result = sut.tokenize(tempFile.toPath())
        Assertions.assertEquals(TokenizationResult.Rejected, result)
    }

    @Test
    fun `binary file, returns Rejected`() {
        val result = sut.tokenize(binaryFile)
        Assertions.assertEquals(TokenizationResult.Rejected, result)
    }

    @Test
    fun `non-existing file, returns Unreachable`() {
        val nonExistingFile = Paths.get("some_file_which_does_not_exist")

        Assertions.assertEquals(TokenizationResult.Unreachable, sut.tokenize(nonExistingFile))
    }

    @Test
    fun `unreadable file, returns Unreachable`() {
        tempFile.setReadable(false)

        val result = sut.tokenize(tempFile.toPath())
        Assertions.assertEquals(TokenizationResult.Unreachable, result)
    }

    @Test
    fun `has word with punctuation, returns tokens`() {
        writeLine("He  <exclaimed>, 'ololo!'")

        val result = sut.tokenize(tempFile.toPath()) as TokenizationResult.Success
        Assertions.assertTrue(result.tokens.contains("He"))
        Assertions.assertTrue(result.tokens.contains("exclaimed"))
        Assertions.assertTrue(result.tokens.contains("ololo"))
    }

    @Test
    fun `long text file, parsed successfully`() {
        val result = sut.tokenize(longTextpath) as TokenizationResult.Success

        Assertions.assertEquals(13738, result.tokens.size)
    }
}