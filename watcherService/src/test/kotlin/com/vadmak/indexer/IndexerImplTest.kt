package com.vadmak.indexer

import com.nhaarman.mockitokotlin2.*
import com.vadmak.indexer.index.Index
import com.vadmak.indexer.index.IndexFactory
import com.vadmak.indexer.index.IndexMutationEvent
import com.vadmak.indexer.index.MutableIndex
import com.vadmak.indexer.tokens.Predicate
import com.vadmak.indexer.tokens.PredicateFactory
import com.vadmak.indexer.fs.FileSystem
import com.vadmak.indexer.modifications.FsEvent
import com.vadmak.indexer.modifications.FsModificationsListener
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.PublishSubject
import io.reactivex.rxjava3.subjects.Subject
import org.junit.jupiter.api.*
import java.nio.file.InvalidPathException
import java.nio.file.Path
import java.util.concurrent.TimeUnit

class IndexerImplTest {
    private lateinit var modificationsListenerMock: FsModificationsListener
    private lateinit var modificationsSubject: Subject<List<FsEvent>>
    private lateinit var fsMock: FileSystem
    private lateinit var indexMock: Index
    private lateinit var mutableIndexMock: MutableIndex
    private lateinit var predicateFactoryMock: PredicateFactory

    private lateinit var rootFolder: Path
    private val rootFolderString = "some/path"

    private lateinit var sut: IndexerImpl

    @BeforeEach
    fun setUp() {
        modificationsSubject = PublishSubject.create()
        modificationsListenerMock = mock<FsModificationsListener>().also {
            whenever(it.recursiveWatch(any())).thenReturn(modificationsSubject)
        }

        fsMock = mock()
        indexMock = mock()
        mutableIndexMock = mock()
        predicateFactoryMock = mock()
        rootFolder = mock()

        whenever(fsMock.isSymlink(eq(rootFolder))).thenReturn(false)
        whenever(fsMock.isDirectory(eq(rootFolder))).thenReturn(true)
        whenever(rootFolder.toString()).thenReturn(rootFolderString)
        whenever(fsMock.getPath(eq(rootFolderString))).thenReturn(rootFolder)

        val indexFactory = mock<IndexFactory>().also {
            whenever(it.construct()).thenReturn(indexMock)
        }

        sut = IndexerImpl(modificationsListenerMock, fsMock, indexFactory, predicateFactoryMock)

        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setSingleSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
    }

    @AfterEach
    fun tearDown() {
        RxJavaPlugins.setIoSchedulerHandler(null)
        RxJavaPlugins.setSingleSchedulerHandler(null)
        RxJavaPlugins.setComputationSchedulerHandler(null)
    }

    @Test
    fun `addFolder with invalid path, exception is thrown`() {
        val invalidPathString = "someInvalidPath"
        whenever(fsMock.getPath(eq(invalidPathString))).thenThrow(InvalidPathException("", ""))

        Assertions.assertThrows(InvalidPathException::class.java) {
            sut.addFolder(invalidPathString).blockingAwait()
        }
    }

    @Test
    fun `addFolder of not a folder, exception is thrown`() {
        val invalidPathString = "someInvalidPath"
        mock<Path>().let {
            whenever(fsMock.getPath(eq(invalidPathString))).thenReturn(it)
            whenever(fsMock.isDirectory(eq(it))).thenReturn(false)
        }

        Assertions.assertThrows(InvalidPathException::class.java) {
            sut.addFolder(invalidPathString).blockingAwait()
        }
    }

    @Nested
    inner class AfterFolderAdded {
        private lateinit var predicate: Predicate
        private var snapshotInvocationsCount = 0

        @BeforeEach
        fun setUp() {
            predicate = mock()
            whenever(predicateFactoryMock.construct(any())).thenReturn(predicate)

            whenever(indexMock.startMutation()).thenReturn(mutableIndexMock)
            whenever(mutableIndexMock.commit()).thenReturn(indexMock)
            whenever(indexMock.toSnapshot(eq(predicate)))
                .then {
                    // this is made to bypass the `distinctUntilChanged`
                    Indexer.Snapshot(listOf(snapshotInvocationsCount++.toString()))
                }
            whenever(mutableIndexMock.applyEvent(any())).then {
                when (val mutationEvent = it.arguments.first() as IndexMutationEvent) {
                    is IndexMutationEvent.ArtificialFolderAdd -> mutationEvent.onFolderAdded()
                    is IndexMutationEvent.ArtificialFolderRemove -> mutationEvent.onFolderRemoved()
                    else -> Unit
                }
            }

            sut.addFolder(rootFolderString).blockingAwait()
        }

        @Test
        fun `fsModificationsListener called`() {
            verify(modificationsListenerMock).recursiveWatch(rootFolderString)
        }

        @Test
        fun `listAddedFolder will return added folder`() {
            val folders = sut.listAddedFolders().test()

            folders.awaitCount(1)
            folders.assertValue(rootFolderString)
        }

        @Nested
        inner class StartedSearch {

            private val matcher = Matcher.PlainWord("ololo")

            @BeforeEach
            fun setUp() {
            }

            @Test
            fun `index mutation performed with artificial event`() {
                sut.search(matcher).test().awaitCount(1)

                val callingOrder = inOrder(indexMock, mutableIndexMock)
                callingOrder.verify(indexMock).startMutation()
                callingOrder.verify(mutableIndexMock).applyEvent(isA<IndexMutationEvent.ArtificialFolderAdd>())
                callingOrder.verify(mutableIndexMock).visitUnvisited(any())
                callingOrder.verify(mutableIndexMock).commit()
            }

            @Test
            fun `got fsChange, index mutation performed`() {
                val testObserver = sut.search(matcher).test()
                testObserver.awaitCount(1)

                val randomPath = mock<Path>()
                val fsEvents = listOf(FsEvent(FsEvent.Type.Created, randomPath))

                modificationsSubject.onNext(fsEvents)
                testObserver.awaitCount(2)

                val callingOrder = inOrder(indexMock, mutableIndexMock)
                callingOrder.verify(indexMock).startMutation()
                callingOrder.verify(mutableIndexMock).applyEvent(IndexMutationEvent.FromFileSystem(fsEvents))
                callingOrder.verify(mutableIndexMock).visitUnvisited(any())
                callingOrder.verify(mutableIndexMock).commit()
            }

            @Test
            fun `closing indexer will terminate search stream`() {
                val testObserver = sut.search(matcher).test()
                testObserver.awaitCount(1)

                sut.close()
                testObserver
                    .awaitDone(1, TimeUnit.SECONDS)
                    .assertComplete()
                Assertions.assertFalse(modificationsSubject.hasObservers())
            }

            @Test
            fun `removeFolder with non added folder, NoSuchElementException is thrown`() {
                val operation = sut.removeFolder("someRandomString")

                Assertions.assertThrows(NoSuchElementException::class.java) {
                    operation.blockingAwait()
                }
            }

            @Test
            fun `removeFolder of added one, index mutation performed with artificial event`() {
                sut.search(matcher).test().awaitCount(1)
                sut.removeFolder(rootFolderString).blockingAwait()

                val callingOrder = inOrder(indexMock, mutableIndexMock)
                callingOrder.verify(indexMock).startMutation()
                callingOrder.verify(mutableIndexMock).applyEvent(isA<IndexMutationEvent.ArtificialFolderRemove>())
                callingOrder.verify(mutableIndexMock).visitUnvisited(any())
                callingOrder.verify(mutableIndexMock).commit()
            }
        }
    }
}