package com.vadmak.indexer.modifications

import com.nhaarman.mockitokotlin2.*
import com.vadmak.indexer.fs.FileSystem
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import javaxt.io.Directory
import org.junit.jupiter.api.*
import java.nio.file.Path

internal class ExposedEvent(eventType: FsEvent.Type, filePathString: String):
    Directory.Event(stringifyEventType(eventType), filePathString) {
    companion object {
        fun stringifyEventType(eventType: FsEvent.Type) = when(eventType) {
            FsEvent.Type.Created -> "create"
            FsEvent.Type.Deleted -> "delete"
            FsEvent.Type.Modified -> "modify"
        }
    }
}

class FsModificationsListenerExtTest {
    private lateinit var fsMock: FileSystem
    private lateinit var directory: Directory
    private val unsafeList = java.util.ArrayList<Directory.Event?>()
    private lateinit var sut: FsModificationsListenersExt

    private val dirPathString = "some/path"

    @BeforeEach
    fun setUp() {
        directory = mock()
        fsMock = mock()

        whenever(fsMock.createDirectory(any())).thenReturn(directory)
        whenever(directory.events).thenReturn(unsafeList)

        sut = FsModificationsListenersExt(fsMock)
    }

    @Test
    fun `stream left unsubscribed, directory not created`() {
        sut.recursiveWatch(dirPathString)

        verifyZeroInteractions(fsMock)
    }

    @Nested
    inner class TrampolineTests {
        @BeforeEach
        fun setUp() {
            RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        }

        @AfterEach
        fun tearDown() {
            RxJavaPlugins.setIoSchedulerHandler(null)
        }

        @Test
        fun `stream subscribed, directory created`() {
            unsafeList.add(null)

            sut.recursiveWatch(dirPathString)
                .test()

            verify(fsMock).createDirectory(dirPathString)
        }

        @Test
        fun `stream unsubscribed, directory stopped`() {
            unsafeList.add(null)

            sut.recursiveWatch(dirPathString)
                .test()
                .dispose()

            verify(directory).stop()
        }
    }

    @Nested
    inner class FileEventReceived {
        val filePathString = "some/file"
        lateinit var filePath: Path

        @BeforeEach
        fun setUp() {
            filePath = mock()

            whenever(filePath.toString()).thenReturn(filePathString)
            whenever(fsMock.getPath(eq(filePathString))).thenReturn(filePath)

        }

        @Test
        fun `got file creation event, event emitted`() {
            unsafeList.add(ExposedEvent(FsEvent.Type.Created, filePathString))

            val testObserver = sut.recursiveWatch(dirPathString)
                .test()

            testObserver.awaitCount(1)
                .assertValue(listOf(
                    FsEvent(FsEvent.Type.Created, filePath)
                ))
        }

        @Test
        fun `got file mofication event, event emitted`() {
            unsafeList.add(ExposedEvent(FsEvent.Type.Modified, filePathString))

            val testObserver = sut.recursiveWatch(dirPathString)
                .test()

            testObserver.awaitCount(1)
                .assertValue(listOf(
                    FsEvent(FsEvent.Type.Modified, filePath)
                ))
        }

        @Test
        fun `got file removal event, event emitted`() {
            unsafeList.add(ExposedEvent(FsEvent.Type.Deleted, filePathString))

            val testObserver = sut.recursiveWatch(dirPathString)
                .test()

            testObserver.awaitCount(1)
                .assertValue(listOf(
                    FsEvent(FsEvent.Type.Deleted, filePath)
                ))
        }
    }
}