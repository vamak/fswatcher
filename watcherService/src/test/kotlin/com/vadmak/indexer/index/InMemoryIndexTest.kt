package com.vadmak.indexer.index

import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.vadmak.indexer.Matcher
import com.vadmak.indexer.tokens.ByWordPredicate
import com.vadmak.indexer.tokens.Tokenizer
import com.vadmak.indexer.fs.FileSystem
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.nio.file.Path

class InMemoryIndexTest {
    private lateinit var fsMock: FileSystem
    private lateinit var tokenizerMock: Tokenizer
    private lateinit var sut: Index

    @BeforeEach
    fun setUp() {
        fsMock = mock()
        tokenizerMock = mock()
    }

    private fun Index.intrusivelyGetIndex() = AbstractIndex.accessIndex(this as AbstractIndex)
    private fun MutableIndex.intrusivelyGetIndex() = AbstractIndex.accessIndex(this as AbstractIndex)

    fun <K, V> hashMapOf(vararg pairs: Pair<K, V>): HashMap<K, V> =
        HashMap<K, V>().apply { putAll(pairs) }

    @Nested
    inner class EmptyIndex {
        @BeforeEach
        fun setUp() {
            sut = InMemoryIndex.createEmpty(fsMock, tokenizerMock)
        }

        @Test
        fun `createEmpty creates empty index`() {
            Assertions.assertTrue(sut.intrusivelyGetIndex().isEmpty())
        }

        @Test
        fun `produces empty snapshot`() {
            val snapshot = sut.toSnapshot(ByWordPredicate(Matcher.PlainWord("asd")))

            Assertions.assertTrue(snapshot.files.isEmpty())
        }
    }

    @Nested
    inner class NonEmptyIndex {
        private lateinit var rootFolderPath: Path
        private lateinit var localFilePath: Path

        val filePathString = "/home/user/superBestFile"

        @BeforeEach
        fun setUp() {
            rootFolderPath = mock()
            localFilePath = mock<Path>().also {
                val fullFilePath = mock<Path>()
                whenever(fullFilePath.toString()).thenReturn(filePathString)
                whenever(rootFolderPath.resolve(eq(it))).thenReturn(fullFilePath)
            }

            val dirIndex = AbstractIndex.DirectoryIndex(
                subfolders = emptySet(),
                fileStates = mapOf(localFilePath to AbstractIndex.FileState.Visited),
                invertedTokenIndex = mapOf("token" to setOf(localFilePath))
            )

            val directories = hashMapOf(
                rootFolderPath to dirIndex
            )
            sut = InMemoryIndex(fsMock, directories, tokenizerMock, mutableSetOf(rootFolderPath))
        }

        @Test
        fun `snapshot by correct token contains file path`() {
            val snapshot = sut.toSnapshot(ByWordPredicate(Matcher.PlainWord("token")))

            Assertions.assertEquals(listOf(filePathString), snapshot.files)
        }

        @Test
        fun `snapshot by incorrect token is empty`() {
            val snapshot = sut.toSnapshot(ByWordPredicate(Matcher.PlainWord("random")))

            Assertions.assertTrue(snapshot.files.isEmpty())
        }

        @Test
        fun `created MutableIndex has same directories contents`() {
            val mutableIndex = sut.startMutation()

            Assertions.assertEquals(sut.intrusivelyGetIndex(), mutableIndex.intrusivelyGetIndex())
        }

        @Test
        fun `created MutableIndex has different directories container`() {
            val mutableIndex = sut.startMutation()

            Assertions.assertFalse(sut.intrusivelyGetIndex() === mutableIndex.intrusivelyGetIndex())
        }
    }
}