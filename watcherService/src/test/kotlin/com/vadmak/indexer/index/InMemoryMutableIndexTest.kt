package com.vadmak.indexer.index

import com.nhaarman.mockitokotlin2.*
import com.vadmak.indexer.tokens.TokenizationResult
import com.vadmak.indexer.tokens.Tokenizer
import com.vadmak.indexer.fs.FileSystem
import com.vadmak.indexer.modifications.FsEvent
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.jupiter.api.*
import java.nio.file.Path

class InMemoryMutableIndexTest {
    private lateinit var fsMock: FileSystem
    private lateinit var tokenizerMock: Tokenizer
    private lateinit var sut: MutableIndex

    private lateinit var rootFolderPath: Path
    private lateinit var fileLocalPath: Path
    private lateinit var fileFullPath: Path

    private val emptyExploreFolderResult = FileSystem.FolderExploreResult(emptySequence(), emptySequence())

    fun <K, V> hashMapOf(vararg pairs: Pair<K, V>): HashMap<K, V> =
        HashMap<K, V>().apply { putAll(pairs) }

    @BeforeEach
    fun setUp() {
        fsMock = mock()
        tokenizerMock = mock()

        rootFolderPath = createFolder()
        fileLocalPath = createFile()
        fileFullPath = createFile()
    }

    private fun createFolder(): Path {
        return mock<Path>().also {
            whenever(fsMock.isSymlink(eq(it))).thenReturn(false)
            whenever(fsMock.isDirectory(eq(it))).thenReturn(true)
        }
    }

    private fun createFile(): Path {
        return mock<Path>().also {
            whenever(fsMock.isSymlink(eq(it))).thenReturn(false)
            whenever(fsMock.isDirectory(eq(it))).thenReturn(false)
        }
    }

    private fun Path.mockLast(toBeReturned: Path) {
        whenever(this.iterator()).thenReturn(
            mutableListOf(toBeReturned).iterator()
        )
    }

    data class PathPair(val fullPath: Path, val localPath: Path)

    private fun createFolder(parentFolder: Path): PathPair {
        val fullPath = createFolder()
        val localPath = createFolder()

        fullPath.mockLast(localPath)
        whenever(parentFolder.resolve(eq(localPath))).thenReturn(fullPath)
        whenever(fullPath.parent).thenReturn(parentFolder)

        return PathPair(fullPath, localPath)
    }

    private fun createFile(parentFolder: Path): PathPair {
        val fullPath = createFile()
        val localPath = createFile()

        fullPath.mockLast(localPath)
        whenever(parentFolder.resolve(eq(localPath))).thenReturn(fullPath)
        whenever(fullPath.parent).thenReturn(parentFolder)

        return PathPair(fullPath, localPath)
    }


    private fun MutableIndex.intrusivelyGetIndex() = AbstractIndex.accessIndex(this as AbstractIndex)
    private fun FsEvent.wrap() = IndexMutationEvent.FromFileSystem(listOf(this))

    @Nested
    @DisplayName("visitUnvisited")
    inner class VisitUnvisited {
        private fun createSutWithFileState(state: AbstractIndex.FileState) {
            val dirIndex = AbstractIndex.DirectoryIndex(
                subfolders = emptySet(),
                fileStates = mapOf(fileLocalPath to state),
                invertedTokenIndex = mapOf("token" to setOf(fileLocalPath))
            )
            whenever(rootFolderPath.resolve(eq(fileLocalPath))).thenReturn(fileFullPath)

            val directories = hashMapOf(
                rootFolderPath to dirIndex
            )
            val addedFolders = mutableSetOf(rootFolderPath)
            sut = InMemoryMutableIndex(fsMock, directories, tokenizerMock, addedFolders)
        }

        @Test
        fun `visited file is not processed`() {
            createSutWithFileState(AbstractIndex.FileState.Visited)

            val index = sut.intrusivelyGetIndex()
            sut.visitUnvisited(Schedulers.trampoline())

            Assertions.assertEquals(index, sut.intrusivelyGetIndex())
            verifyZeroInteractions(tokenizerMock)
        }

        @Test
        fun `unreachable file is not processed`() {
            createSutWithFileState(AbstractIndex.FileState.Unreachable)

            val index = sut.intrusivelyGetIndex()
            sut.visitUnvisited(Schedulers.trampoline())

            Assertions.assertEquals(index, sut.intrusivelyGetIndex())
            verifyZeroInteractions(tokenizerMock)
        }

        @Nested
        @DisplayName("unvisited file is processed, tokenization unsuccessful")
        inner class UnvisitedFileProcessedTokenizationUnsuccessful {
            private lateinit var previousDirIndex: AbstractIndex.DirectoryIndex

            @BeforeEach
            fun setUp() {
                createSutWithFileState(AbstractIndex.FileState.ToBeVisited)
                whenever(tokenizerMock.tokenize(fileFullPath)).thenReturn(TokenizationResult.Unreachable)

                previousDirIndex = sut.intrusivelyGetIndex()[rootFolderPath]!!
                sut.visitUnvisited(Schedulers.trampoline())
            }

            @Test
            fun `file marked unreachable`() {
                verify(tokenizerMock).tokenize(fileFullPath)
                with(sut.intrusivelyGetIndex()[rootFolderPath]!!) {
                    Assertions.assertEquals(AbstractIndex.FileState.Unreachable, this.fileStates[fileLocalPath])
                }
            }

            @Test
            fun `updated directory index is another instance`() {
                with(sut.intrusivelyGetIndex()[rootFolderPath]!!) {
                    Assertions.assertFalse(previousDirIndex === this)
                }
            }
        }

        @Nested
        @DisplayName("unvisited file is processed, tokenization unsuccessful")
        inner class UnvisitedFileProcessedTokenizationSuccessful {
            private lateinit var previousDirIndex: AbstractIndex.DirectoryIndex
            val newToken = "duckTales"

            @BeforeEach
            fun setUp() {
                createSutWithFileState(AbstractIndex.FileState.ToBeVisited)
                whenever(tokenizerMock.tokenize(fileFullPath)).thenReturn(TokenizationResult.Success(setOf(newToken)))

                previousDirIndex = sut.intrusivelyGetIndex()[rootFolderPath]!!
                sut.visitUnvisited(Schedulers.trampoline())
            }

            @Test
            fun `file marked visited`() {
                verify(tokenizerMock).tokenize(fileFullPath)
                with(sut.intrusivelyGetIndex()[rootFolderPath]!!) {
                    Assertions.assertEquals(AbstractIndex.FileState.Visited, this.fileStates[fileLocalPath])
                }
            }

            @Test
            fun `new token points to file`() {
                val newDirIndex = sut.intrusivelyGetIndex()[rootFolderPath]!!
                Assertions.assertEquals(setOf(fileLocalPath), newDirIndex.invertedTokenIndex[newToken])
            }

            @Test
            fun `updated directory index is another instance`() {
                with(sut.intrusivelyGetIndex()[rootFolderPath]!!) {
                    Assertions.assertFalse(previousDirIndex === this)
                }
            }
        }
    }

    @Nested
    inner class EntryCreated {

        @BeforeEach
        fun setUp() {
            val rootDirIndex = AbstractIndex.DirectoryIndex()

            sut = InMemoryMutableIndex(fsMock, hashMapOf(rootFolderPath to rootDirIndex), tokenizerMock, mutableSetOf(rootFolderPath))

            whenever(rootFolderPath.resolve(eq(fileLocalPath))).thenReturn(fileFullPath)
            whenever(fileFullPath.parent).thenReturn(rootFolderPath)
            fileFullPath.mockLast(fileLocalPath)
        }

        @Test
        fun `added symlink, it is ignored`() {
            whenever(fsMock.isSymlink(eq(fileLocalPath))).thenReturn(true)

            sut.applyEvent(FsEvent(FsEvent.Type.Created, fileLocalPath).wrap())
            Assertions.assertTrue(sut.intrusivelyGetIndex().getValue(rootFolderPath).fileStates.isEmpty())
        }

        @Test
        fun `added file, it is labeled ToBeVisited`() {
            sut.applyEvent(FsEvent(FsEvent.Type.Created, fileFullPath).wrap())

            with(sut.intrusivelyGetIndex().getValue(rootFolderPath)) {
                Assertions.assertEquals(AbstractIndex.FileState.ToBeVisited, this.fileStates[fileLocalPath])
            }
        }

        @Test
        fun `empty index, empty folder added, it is included into index`() {
            val (emptyFolderFullPath, emptyFolderLocalPath) = createFolder(rootFolderPath)

            whenever(fsMock.exploreFolder(eq(emptyFolderFullPath))).thenReturn(emptyExploreFolderResult)

            sut = InMemoryMutableIndex(fsMock, hashMapOf(), tokenizerMock, mutableSetOf())
            sut.applyEvent(FsEvent(FsEvent.Type.Created, emptyFolderFullPath).wrap())

            with(sut.intrusivelyGetIndex()) {
                Assertions.assertEquals(1, this.size)
                Assertions.assertTrue(this.getValue(emptyFolderFullPath).fileStates.isEmpty())
                Assertions.assertTrue(this.getValue(emptyFolderFullPath).subfolders.isEmpty())
            }
        }

        @Test
        fun `empty index, artificially added folder, it is included into index`() {
            val (emptyFolderFullPath, emptyFolderLocalPath) = createFolder(rootFolderPath)
            whenever(fsMock.exploreFolder(eq(emptyFolderFullPath))).thenReturn(emptyExploreFolderResult)

            sut = InMemoryMutableIndex(fsMock, hashMapOf(), tokenizerMock, mutableSetOf())
            sut.applyEvent(IndexMutationEvent.ArtificialFolderAdd(emptyFolderFullPath) {})

            with(sut.intrusivelyGetIndex()) {
                Assertions.assertEquals(1, this.size)
                Assertions.assertTrue(this.getValue(emptyFolderFullPath).fileStates.isEmpty())
                Assertions.assertTrue(this.getValue(emptyFolderFullPath).subfolders.isEmpty())
            }
        }

        @Test
        fun `added empty folder, it is included into index`() {
            val (emptyFolderFullPath, emptyFolderLocalPath) = createFolder(rootFolderPath)
            whenever(fsMock.exploreFolder(eq(emptyFolderFullPath))).thenReturn(emptyExploreFolderResult)

            sut.applyEvent(FsEvent(FsEvent.Type.Created, emptyFolderFullPath).wrap())

            with(sut.intrusivelyGetIndex()) {
                Assertions.assertEquals(2, this.size)
                Assertions.assertTrue(this.getValue(emptyFolderFullPath).fileStates.isEmpty())
                Assertions.assertTrue(this.getValue(emptyFolderFullPath).subfolders.isEmpty())

                Assertions.assertEquals(setOf(emptyFolderLocalPath), this.getValue(rootFolderPath).subfolders)
            }
        }

        @Test
        fun `added nested folder with file, both included into index`() {
            val (newFolderFullPath, newFolderLocalPath) = createFolder(rootFolderPath)
            val (newFileFullPath, newFileLocalPath) = createFile(newFolderFullPath)

            whenever(fsMock.exploreFolder(eq(newFolderFullPath)))
                .thenReturn(FileSystem.FolderExploreResult(sequenceOf(newFileFullPath), emptySequence()))

            sut.applyEvent(FsEvent(FsEvent.Type.Created, newFolderFullPath).wrap())

            Assertions.assertEquals(2, sut.intrusivelyGetIndex().size)
            with(sut.intrusivelyGetIndex().getValue(rootFolderPath)) {
                Assertions.assertEquals(setOf(newFolderLocalPath), this.subfolders)
            }
            with(sut.intrusivelyGetIndex().getValue(newFolderFullPath)) {
                Assertions.assertTrue(this.subfolders.isEmpty())
                Assertions.assertEquals(AbstractIndex.FileState.ToBeVisited, this.fileStates[newFileLocalPath])
            }
        }
    }

    @Nested
    inner class EntryModified {
        val onefileToken = "superToken"
        val sharedToken = "awesomeToken"

        private lateinit var filePath2Local: Path
        private lateinit var filePath2Full: Path

        @BeforeEach
        fun setUp() {
            with(createFile(rootFolderPath)) {
                fileLocalPath = this.localPath
                fileFullPath = this.fullPath
            }
            with(createFile(rootFolderPath)) {
                filePath2Local = this.localPath
                filePath2Full = this.fullPath
            }


            val rootDirIndex = AbstractIndex.DirectoryIndex(
                subfolders = emptySet(),
                fileStates = mapOf(
                    fileLocalPath to AbstractIndex.FileState.ToBeVisited,
                    filePath2Local to AbstractIndex.FileState.ToBeVisited
                ),
                invertedTokenIndex = mapOf(
                    onefileToken to setOf(fileLocalPath),
                    sharedToken to setOf(fileLocalPath, filePath2Local)
                )
            )
            sut = InMemoryMutableIndex(fsMock, hashMapOf(rootFolderPath to rootDirIndex), tokenizerMock, mutableSetOf(rootFolderPath))
        }

        @Test
        fun `modified symlink, nothing is changed`() {
            val someSymlink = mock<Path>().also {
                whenever(fsMock.isSymlink(eq(it))).thenReturn(true)
            }
            val index = sut.intrusivelyGetIndex()

            sut.applyEvent(FsEvent(FsEvent.Type.Modified, someSymlink).wrap())

            Assertions.assertEquals(index, sut.intrusivelyGetIndex())
            Assertions.assertTrue(index === sut.intrusivelyGetIndex())
        }

        @Test
        fun `modified existing folder, nothing is changed`() {
            val index = sut.intrusivelyGetIndex()

            sut.applyEvent(FsEvent(FsEvent.Type.Modified, rootFolderPath).wrap())

            Assertions.assertEquals(index, sut.intrusivelyGetIndex())
            Assertions.assertTrue(index === sut.intrusivelyGetIndex())
        }

        @Test
        fun `modified non-existing folder, included into index`() {
            val newEmptyFolder = createFolder(rootFolderPath)
            whenever(fsMock.exploreFolder(eq(newEmptyFolder.fullPath))).thenReturn(emptyExploreFolderResult)

            sut.applyEvent(FsEvent(FsEvent.Type.Modified, newEmptyFolder.fullPath).wrap())

            with(sut.intrusivelyGetIndex().getValue(rootFolderPath)) {
                Assertions.assertEquals(setOf(newEmptyFolder.localPath), this.subfolders)
            }
            with(sut.intrusivelyGetIndex().getValue(newEmptyFolder.fullPath)) {
                Assertions.assertTrue(this.subfolders.isEmpty())
                Assertions.assertTrue(this.fileStates.isEmpty())
                Assertions.assertTrue(this.invertedTokenIndex.isEmpty())
            }
        }

        @Test
        fun `modified existing file, marked as ToBeVisited`() {
            sut.applyEvent(FsEvent(FsEvent.Type.Modified, fileFullPath).wrap())

            with(sut.intrusivelyGetIndex().getValue(rootFolderPath)) {
                Assertions.assertEquals(AbstractIndex.FileState.ToBeVisited, this.fileStates[fileLocalPath])
            }
        }

        @Test
        fun `modified existing file, new instance of DirectoryIndex in the index`() {
            val oldIndex = sut.intrusivelyGetIndex().getValue(rootFolderPath)
            sut.applyEvent(FsEvent(FsEvent.Type.Modified, fileFullPath).wrap())

            val newIndex = sut.intrusivelyGetIndex().getValue(rootFolderPath)
            Assertions.assertFalse(oldIndex === newIndex)
        }

        @Test
        fun `modified existing file, deleted from invertedTokenIndex`() {
            with(sut.intrusivelyGetIndex().getValue(rootFolderPath)) {
                Assertions.assertEquals(setOf(fileLocalPath), this.invertedTokenIndex[onefileToken])
            }

            sut.applyEvent(FsEvent(FsEvent.Type.Modified, fileFullPath).wrap())

            with(sut.intrusivelyGetIndex().getValue(rootFolderPath)) {
                Assertions.assertEquals(setOf(filePath2Local), this.invertedTokenIndex[sharedToken])
                Assertions.assertFalse(this.invertedTokenIndex.containsKey(onefileToken))
            }
        }
    }

    @Nested
    inner class EntryDeleted {
        private lateinit var innerFolder: PathPair
        private val token = "yeah"

        @BeforeEach
        fun setUp() {
            innerFolder = createFolder(rootFolderPath)

            with(createFile(innerFolder.fullPath)) {
                fileLocalPath = this.localPath
                fileFullPath = this.fullPath
            }

            val rootDirIndex = AbstractIndex.DirectoryIndex(
                subfolders = setOf(innerFolder.localPath)
            )
            val innerFolderIndex = AbstractIndex.DirectoryIndex(
                fileStates = mapOf(fileLocalPath to AbstractIndex.FileState.Visited),
                invertedTokenIndex = mapOf(token to setOf(fileLocalPath))
            )

            sut = InMemoryMutableIndex(
                fsMock,
                hashMapOf(rootFolderPath to rootDirIndex, innerFolder.fullPath to innerFolderIndex),
                tokenizerMock,
                mutableSetOf(rootFolderPath)
            )
        }

        @Test
        fun `unknown file deleted, nothing is changed`() {
            val unknownFile = createFile(innerFolder.fullPath)
            val index = sut.intrusivelyGetIndex()

            sut.applyEvent(FsEvent(FsEvent.Type.Deleted, unknownFile.fullPath).wrap())

            Assertions.assertEquals(index, sut.intrusivelyGetIndex())
            Assertions.assertTrue(index === sut.intrusivelyGetIndex())
        }

        @Test
        fun `file is deleted, it's gone from index`() {
            sut.applyEvent(FsEvent(FsEvent.Type.Deleted, fileFullPath).wrap())

            with(sut.intrusivelyGetIndex().getValue(innerFolder.fullPath)) {
                Assertions.assertTrue(this.fileStates.isEmpty())
                Assertions.assertTrue(this.invertedTokenIndex.isEmpty())
            }
        }

        @Test
        fun `inner folder is deleted, subtree is gone from index`() {
            sut.applyEvent(FsEvent(FsEvent.Type.Deleted, innerFolder.fullPath).wrap())

            Assertions.assertFalse(sut.intrusivelyGetIndex().containsKey(innerFolder.fullPath))
            with(sut.intrusivelyGetIndex().getValue(rootFolderPath)) {
                Assertions.assertTrue(this.subfolders.isEmpty())
            }
        }

        @Test
        fun `root folder is deleted, index is empty`() {
            whenever(rootFolderPath.parent).thenReturn(mock<Path>())
            rootFolderPath.mockLast(mock<Path>())
            sut.applyEvent(FsEvent(FsEvent.Type.Deleted, rootFolderPath).wrap())

            Assertions.assertTrue(sut.intrusivelyGetIndex().isEmpty())
        }
    }
}